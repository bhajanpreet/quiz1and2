package com.quiz1and2;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		/**
		 * Part 1:
		 *  Ask user to input the number of weeks they want to calculate the infection rate for
			Output how many instructors are infected at the end of each week
			At the end of your program, output the total number of infected instructors.
		 */
		int numberInfected = 0;
		int totalInfected = 0;
		int infectionsPerWeek = 5 * 7;
		int infectionsAfter8Days = 8 * 7;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of weeks they want to calculate the infection rate for!");
		int weeks = sc.nextInt();
		
		if(weeks < 1) {
			System.out.println("Wrong Input");
		}else {
			for (int i = 0; i <= weeks; i++) {
				if(i == 0) {
					numberInfected = 1;
				}else if(i % 2 == 0) {
					numberInfected = 0;
				}
				else if(i <= 8){
					numberInfected = infectionsPerWeek;
				}else if(i > 8) {
					numberInfected = infectionsAfter8Days;
				}
				totalInfected += numberInfected;
				System.out.println("Infected persons at end of week "+i+" are:"+totalInfected);
			}
		}
		System.out.println("Infected persons at end of program are: "+totalInfected);
		sc.close();
	}
}
